#include <vector>
#include <map>
#include <iostream>
#include <map>
using namespace std;

const vector<int> puzzle = {3,5,1,2,5,4,1,5,1,2,5,5,1,3,1,5,1,3,2,1,5,1,1,1,2,3,1,3,1,2,1,1,5,1,5,4,5,5,3,3,1,5,1,1,5,5,1,3,5,5,3,2,2,4,1,5,3,4,2,5,4,1,2,2,5,1,1,2,4,4,1,3,1,3,1,1,2,2,1,1,5,1,1,4,4,5,5,1,2,1,4,1,1,4,4,3,4,2,2,3,3,2,1,3,3,2,1,1,1,2,1,4,2,2,1,5,5,3,4,5,5,2,5,2,2,5,3,3,1,2,4,2,1,5,1,1,2,3,5,5,1,1,5,5,1,4,5,3,5,2,3,2,4,3,1,4,2,5,1,3,2,1,1,3,4,2,1,1,1,1,2,1,4,3,1,3,1,2,4,1,2,4,3,2,3,5,5,3,3,1,2,3,4,5,2,4,5,1,1,1,4,5,3,5,3,5,1,1,5,1,5,3,1,2,3,4,1,1,4,1,2,4,1,5,4,1,5,4,2,1,5,2,1,3,5,5,4,5,5,1,1,4,1,2,3,5,3,3,1,1,1,4,3,1,1,4,1,5,3,5,1,4,2,5,1,1,4,4,4,2,5,1,2,5,2,1,3,1,5,1,2,1,1,5,2,4,2,1,3,5,5,4,1,1,1,5,5,2,1,1};
const vector<int> example = {3,4,3,1,2};

struct Key {
    int initial;
    int days_left;

    bool operator==(const Key &other) const {
        return ((initial == other.initial) && (days_left == other.days_left));
    }

    bool operator<(const Key &other) const {
        if(initial == other.initial) return days_left < other.days_left;
        return initial < other.initial;
    }
};

const int TOTAL_DAYS = 256;

unsigned long long count_fish(int initial, int days_left) {
    static map<Key, long long> memo;
    Key k = {initial, days_left};

    // Memoize the function for initial
    if(memo.find(k) != memo.end()) {
        return memo[k];
    }
    
    // The total number of fish for a given fish is this fish,
    // plus all this kids that this fish has, plus all the kids 
    // that those kids have
    unsigned long long total = 1;

    // Once they start having fish (the day after they reach 0), they will have a 
    // new one every 7 days.
    for(int days_remaining = days_left - (initial + 1); days_remaining >= 0; days_remaining = days_remaining - 7) {
        total += count_fish(8, days_remaining);
    }

    memo[k] = total;
    return total;
}

long long problem_2(vector<int> inital) {
    long long total = 0;
    for(auto a: inital) {
        total += count_fish(a, TOTAL_DAYS);
    }
    return total;
}

long long problem_1(vector<int> initial) {
    for(int i = 0; i < 80; i++)  {
        int kids = 0;
        for(size_t i = 0; i < initial.size(); i++) {
            int next = initial[i] - 1;
            if(next < 0) {
                kids++;
                next = 6;
            }
            initial[i] = next;
        }
        for(int i = 0; i < kids; i++) {
            initial.push_back(8);
        }
    }
    return initial.size();
}

int main() {
    cout << "Problem 1: " << problem_1(puzzle) << endl;
    cout << "Problem 2: " << problem_2(puzzle) << endl;
    return 0;
}