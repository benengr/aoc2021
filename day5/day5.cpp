#include <vector>
#include <string>
#include <map>
#include <fstream>
#include <iostream>

using namespace std;

struct Point {
    int x;
    int y;

    bool operator==(const Point &o) const {
        return x == o.x && y == o.y;
    }

    bool operator<(const Point &o) const {
        if(y == o.y) {
            return x < o.x;
        } else {
            return y < o.y;
        }
    }
};



struct Line {
    Point a;
    Point b;
};

int problem1(vector<Line> &lines) {
    map<Point, int> results;

    for(auto l : lines) {
        if(l.a.x == l.b.x) {
            int start = min(l.a.y, l.b.y);
            int stop = max(l.a.y, l.b.y);
            for(int i = start; i <= stop; i++) {
                Point p = {
                    l.a.x,
                    i
                };

                if(results.find(p) == results.end()) {
                    results[p] = 1;
                } else {
                    results[p]++;
                }
            }
        } else if(l.a.y == l.b.y) {
            int start = min(l.a.x, l.b.x);
            int stop = max(l.a.x, l.b.x);
            for(int i = start; i <= stop; i++) {
                Point p = {
                    i,
                    l.a.y
                };

                if(results.find(p) == results.end()) {
                    results[p] = 1;
                } else {
                    results[p]++;
                }
            }
        }
    }

    int count = 0;
    for(auto i : results) {
        if(i.second > 1) {
            count++;
        }
    }
    return count;
}


int problem2(vector<Line> &lines) {
    map<Point, int> results;

    for(auto l : lines) {
        int xstart = l.a.x;
        int xend = l.b.x;
        int ystart = l.a.y;
        int yend = l.b.y;
        bool xinc = xend > xstart;
        bool yinc = yend > ystart;

        int x = xstart;
        int y = ystart;
        while(true) {
            Point p {x, y};
            if(results.find(p) == results.end()) {
                results[p] = 1;
            } else {
                results[p]++;
            }

            // This will break of the line does is not 45 degrees
            if((x == xend) && (y == yend)) break;
            if(x != xend) xinc ? x++ : x--;
            if(y != yend) yinc ? y++ : y--;
        }
    }
        

    int count = 0;
    for(auto i : results) {
        if(i.second > 1) {
            count++;
        }
    }
    return count;
}

Line parse(string &s) {
    size_t i = 0;
    int digit = 0;
    while(s[i] != ',') {
        digit = digit * 10 + (s[i] - '0');
        i++;
    }
    int x = digit;
    digit = 0;
    i++;

    while(s[i] != ' ') {
        digit = digit * 10 + (s[i] - '0');
        i++;
    }
    int y = digit;

    Line result;
    result.a = {x, y};
    digit = 0;
    i += 4;
    while(s[i] != ',') {
        digit = digit * 10 + (s[i] - '0');
        i++;
    }
    x = digit;
    digit = 0;
    i++;

    while(i < s.size()) {
        digit = digit * 10 + (s[i] - '0');
        i++;
    }
    y = digit;
    result.b = {x, y};
    return result;
}

int main() {
    vector<Line> data;
    string filename = "puzzle_input.txt";
    ifstream input_file(filename);
    string line;

    if(!input_file.is_open()) {
        cerr << "Could not open " << filename << endl;
        return EXIT_FAILURE;
    }

    while(getline(input_file, line)) {
        data.push_back(parse(line));
    }

    cout << "Running Problem 1" << endl;
    int result = problem1(data);
    cout << "Problem 1 Result: " << result << endl;

    cout << "Running Problem 2" << endl;
    result = problem2(data);
    cout << "Problem 2 Result: " << result << endl;
}