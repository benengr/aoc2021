#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <set>
#include <map>

using namespace std;
const int INF = 9999999;

struct Node {
  uint8_t risk;
  int distance;
  int distance_from_end;
  int id;
  int fscore;
  Node *prev;
  vector<Node *> connections;

  bool operator<(const Node &rhs) {
    if (distance != rhs.distance)
      return distance < rhs.distance;
    return id < rhs.id;
  }
};

struct NodeCmp {
  bool operator()(const Node *lhs, const Node *rhs) const {
    if (lhs->distance != rhs->distance) {
      if (lhs->distance < 0)
        return false;
      if (rhs->distance < 0)
        return true;
      return lhs->distance < rhs->distance;
    };
    return lhs->id < rhs->id;
  }
};

bool sort_node(const Node *lhs, const Node *rhs) {
  if (lhs->distance != rhs->distance) {
    if (lhs->distance < 0)
      return false;
    if (rhs->distance < 0)
      return true;
    return lhs->distance < rhs->distance;
  };
  return lhs->id < rhs->id;
}

struct puzzle_input {
  vector<vector<Node *>> nodes;
};

long long problem1(puzzle_input data) {

  vector<Node *> unvisited;
  int temp = data.nodes[0][0]->risk;
  data.nodes[0][0]->distance = temp;
  for (auto row : data.nodes) {
    for (auto n : row) {
      unvisited.push_back(n);
    }
  }

  while (unvisited.size() > 0) {
    cout << "Unvisized Count: " << unvisited.size() << endl;
    sort(unvisited.begin(), unvisited.end(), sort_node);
    auto u = *unvisited.begin();
    unvisited.erase(unvisited.begin());

    for (auto n : u->connections) {
      int d = u->distance + n->risk;
      if ((n->distance < 0) || (d < n->distance)) {
        n->distance = d;
        n->prev = u;
      }
    }
  }

  size_t r = data.nodes.size() - 1;
  size_t c = data.nodes[0].size() - 1;

  int count = 0;
  cout << "id\trisk\tcount" << endl;
  for (Node *current = data.nodes[r][c]; current != NULL;
       current = current->prev) {
    count += current->risk;
    cout << current->id << "\t" << (int)current->risk << "\t" << count << endl;
  }

  return count;
}

template <typename K, typename V>
V GetWithDefault(const std::map<K,V> & m, const K & key, const V & defVal) {
    typename std::map<K,V>::const_iterator it = m.find(key);
    if(it == m.end()) {
        return defVal;
    } else {
        return it->second;
    }
}

class fscore_comp 
{
public:
    bool operator() (const Node* lhs, const Node* rhs) const {
        if (lhs->fscore != rhs->fscore) return lhs->fscore < rhs->fscore;
        return lhs < rhs;
    }
};

long long a_star(Node *start, Node *end) {
    set<Node *> openSet;
    openSet.insert(start);

    map<Node*, Node*> came_from;
    map<Node *, int> gScore; // This is the cheapest cost currently known to get to this node from start.
    map<Node *, int> fScore; // This is the current best guess at how far the end is;

    // Node for the current best guess, it should be the number of nodes needed to tavel * 5 (average node value).
    // 

    while(openSet.size() > 0) {
        Node* current = *openSet.begin();
        openSet.erase(openSet.begin());
        if(current == end) {
            int result = current->distance;
            int total = 0;
            cout << "id\trisk\tcount" << endl;
            while(current != start) {
                total += current->risk;
                cout << current->id << "\t" << (int)current->risk << "\t" << total << endl;
                current = came_from[current];
            }
            return total;
        }

        for(auto n: current->connections) {
            int tentativeScore = gScore[current] + n->risk;
            if(tentativeScore < GetWithDefault(gScore, n, INF)) {
                came_from[n] = current;
                gScore[n] = tentativeScore;
                fScore[n] = tentativeScore + n->distance_from_end * 5;
                n->fscore = fScore[n];
                if(openSet.find(n) == openSet.end()) {
                    openSet.insert(n);
                }
            }
        }
    }


    return -1;
}

long long problem2(puzzle_input data) {
    int end_row = data.nodes.size() - 1;
    int end_col = data.nodes[end_row].size() - 1;
    data.nodes[0][0]->distance = 0;
    return a_star(data.nodes[0][0], data.nodes[end_row][end_col]);
}

puzzle_input read_file(bool example) {
  puzzle_input result;
  string filename = example ? "example_input.txt" : "puzzle_input.txt";
  ifstream input_file(filename);
  string line;

  if (!input_file.is_open()) {
    cerr << "Could not open " << filename << endl;
    exit(1);
  }

  int id = 0;
  while (getline(input_file, line)) {
    vector<Node *> row;
    for (auto r : line) {
      Node *c = new Node;
      c->distance = -1;
      c->risk = (int)r - (int)'0';
      c->id = id;
      c->prev = NULL;
      row.push_back(c);
      id++;
    }
    result.nodes.push_back(row);
  }

  for (size_t r = 0; r < result.nodes.size(); r++) {
    vector<Node *> row = result.nodes[r];
    for (size_t c = 0; c < row.size(); c++) {
      Node *n = row[c];
      if (r > 0) {
        n->connections.push_back(result.nodes[r - 1][c]);
      }
      if (r < result.nodes.size() - 1) {
        n->connections.push_back(result.nodes[r + 1][c]);
      }

      if (c > 0) {
        n->connections.push_back(result.nodes[r][c - 1]);
      }
      if (c < row.size() - 1) {
        n->connections.push_back(result.nodes[r][c + 1]);
      }
    }
  }
  return result;
}

puzzle_input read_file_2(bool example) {
  puzzle_input result;
  string filename = example ? "example_input.txt" : "puzzle_input.txt";
  ifstream input_file(filename);
  string line;

  if (!input_file.is_open()) {
    cerr << "Could not open " << filename << endl;
    exit(1);
  }

  int id = 0;
  while (getline(input_file, line)) {
    vector<Node *> row;
    for (auto r : line) {
      Node *c = new Node;
      c->distance = 9999999;
      c->risk = (int)r - (int)'0';
      c->id = id;
      c->prev = NULL;
      row.push_back(c);
      id++;
    }

    size_t base_size = row.size();
    size_t max = row.size() * 5;
    while (row.size() < max) {
      int next = row[row.size() - base_size]->risk + 1;
      if (next > 9)
        next = 1;
      Node *c = new Node;
      c->distance = -1;
      c->risk = next;
      c->id = id;
      id++;
      c->prev = NULL;
      row.push_back(c);
    }

    // Fill out the rest
    result.nodes.push_back(row);
  }

  size_t max_row = result.nodes.size() * 5;
  size_t col_size = result.nodes[0].size();
  size_t row_offset = result.nodes.size();
  for (size_t current_row = result.nodes.size(); current_row < max_row;
       current_row++) {
    vector<Node *> row;
    for (size_t col = 0; col < col_size; col++) {
      int next = result.nodes[current_row - row_offset][col]->risk + 1;
      if (next > 9)
        next = 1;
      Node *c = new Node;
      c->distance = -1;
      c->risk = next;
      c->id = id;
      id++;
      c->prev = NULL;
      row.push_back(c);
    }
    result.nodes.push_back(row);
  }

  for (size_t r = 0; r < result.nodes.size(); r++) {
    vector<Node *> row = result.nodes[r];
    for (size_t c = 0; c < row.size(); c++) {
      Node *n = row[c];
      n->distance_from_end = max_row - r + col_size - c;
      n->fscore = INF;
      if (r > 0) {
        n->connections.push_back(result.nodes[r - 1][c]);
      }
      if (r < result.nodes.size() - 1) {
        n->connections.push_back(result.nodes[r + 1][c]);
      }

      if (c > 0) {
        n->connections.push_back(result.nodes[r][c - 1]);
      }
      if (c < row.size() - 1) {
        n->connections.push_back(result.nodes[r][c + 1]);
      }
    }
  }
  return result;
}

int main() {
  //   auto lines = read_file(false);

  //   cout << "Running Problem 1" << endl;
  //   long long result = problem1(lines);
  //   cout << "Problem 1 Result: " << result << endl;

  auto lines2 = read_file_2(false);
  cout << "Running Problem 2" << endl;
  auto result = problem2(lines2);
  cout << "Problem 2 Result: " << result << endl;

  return 0;
}