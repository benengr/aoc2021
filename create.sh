#!/bin/bash
if [ -z "$1" ]
then
    echo "create <number>"
    exit 1
else
    BASE="day$1"
    if [ -d "$BASE" ]
    then
        echo "$BASE already exists"
        exit 2
    else
        echo "Creating $BASE"
        mkdir $BASE
        cp template.cpp $BASE/${BASE}.cpp
        touch $BASE/example_input.txt
        touch $BASE/puzzle_input.txt
        echo "Done"
    fi
fi