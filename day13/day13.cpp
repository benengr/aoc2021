#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <set>

using namespace std;

struct Point {
  int x;
  int y;

  bool operator<(const Point& rhs) const {
      if(y < rhs.y) return true;
      else if (y > rhs.y) return false;
      else return (x < rhs.x);
  }

  bool operator==(const Point& rhs) const {
      return ((x == rhs.x) && (y == rhs.y));
  }
};

struct puzzle_input {
  set<Point> grid;
  int maxX;
  int maxY;
  vector<string> instructions;
};

void print_result(puzzle_input &data, ostream &out) {
    set<Point>::iterator current = data.grid.begin();

    for(int row = 0; row < data.maxY; row++) {
        for(int col = 0; col < data.maxX; col++) {
            Point c = {col, row};
            if(c == *current) {
                out << "X";
                current++;
            } else {
                out << ".";
            }
        }
        out << endl;
    }
    if(current != data.grid.end()) {
        cout << "oh no" << current->x << " " << current->y << endl;
    }
}

int perform_fold(puzzle_input &data, int index) {
    bool y = data.instructions[index][0] == 'y';
    set<Point> newset;

    int val = 0;
    for(size_t i = 2; i < data.instructions[index].size(); i++) {
        val = val * 10 + ((int)data.instructions[index][i] - (int)'0');
    }
    if(y) {
        for(auto p : data.grid) {
            if(p.y > val) {
                // int newy = val - (p.y - val);
                int newy = p.y - (p.y - val) * 2;
                newset.insert({p.x, newy});
            } else {
                newset.insert(p);
            }
        }
        data.maxY = val;
    } else {
        for(auto p : data.grid) {
            if(p.x > val) {
                // int newx = val - (p.x - val);
                int newx = p.x - (p.x - val) * 2;
                if(newx < 0) cout << "ugh1" << endl;
                newset.insert({newx, p.y});                
            } else {
                if(p.x == val) cout << "ugh" << endl;
                newset.insert(p);
            }
        }
        data.maxX = val;
    }
    data.grid.swap(newset);
    return data.grid.size();
} 

void fold_all(puzzle_input &data) {
    cout << "Initial" << endl;
    print_result(data, cout);
    cout << endl;

    for(size_t i = 0; i < data.instructions.size(); i++) {
        perform_fold(data, i);
        cout << "Fold " << i + 1 << endl;
        print_result(data, cout);
        cout << endl;
    }
}

int problem1(puzzle_input &data) { 
    ofstream f;
    f.open("results.txt");
    f << "Before" << endl;
    for(auto p : data.grid) {
        f << p.x << "," << p.y << endl;
    }
    int result = perform_fold(data, 0);
    f << "After" << endl;
    for(auto p : data.grid) {
        f << p.x << "," << p.y << endl;
    }
    f << endl;
    f.close();
    return result;
}

int problem2(puzzle_input &lines) { 
    for(size_t i = 0; i < lines.instructions.size(); i++) {
        perform_fold(lines, i);
    }
    print_result(lines, cout);
    return 0;
}

puzzle_input read_file(bool example) {
  puzzle_input result;
  string filename = example ? "example_input.txt" : "puzzle_input.txt";
  ifstream input_file(filename);
  string line;

  if (!input_file.is_open()) {
    cerr << "Could not open " << filename << endl;
    exit(1);
  }

  result.maxY = 0;
  result.maxX = 0;
  while (getline(input_file, line)) {
    if (line[0] == 'f') {
        result.instructions.push_back(line.substr(11, line.size()));
    } else if(line.size() < 2){
        cout << "ignore" << endl;
    }else {
      int coord = 0;
      Point p;
      for (auto a : line) {
        if (a == ',') {
          p.x = coord;
          coord = 0;
        } else {
          coord = coord * 10 + ((int)a - (int)'0');
        }
      }
      p.y = coord;
      if(p.y == 0) {
          cout << line << endl;
      }
      if (p.x > result.maxX)
        result.maxX = p.x;
      if (p.y > result.maxY)
        result.maxY = p.y;
      result.grid.insert(p);
    }
  }
  result.maxX++;
  result.maxY++;
  return result;
}

int main() {
  auto lines = read_file(false);

  cout << "Running Problem 1" << endl;
  cout << "Initail point count" << lines.grid.size() << endl;
  int result = problem1(lines);
  cout << "Problem 1 Result: " << result << endl;

  cout << "Running Problem 2" << endl;
  lines = read_file(false);
  result = problem2(lines);
  cout << "Problem 2 Result: " << result << endl;

  return 0;
}