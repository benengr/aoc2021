#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

int problem1(vector<vector<int>> &lines) {
    vector<int> results;
    for(size_t row = 1; row < lines.size() - 1; row++) {
        vector<int> row_data = lines[row];
        for(size_t col = 1; col < row_data.size() - 1; col++) {
            int uut = lines[row][col];
            if((uut < lines[row + 1][col]) &&
                (uut < lines[row - 1][col]) &&
                (uut < lines[row][col + 1]) &&
                (uut < lines[row][col - 1])) {
                    results.push_back(uut + 1);
                }
        }
    }
    int result = 0;
    for(auto i : results) {
        result += i;
    }
    return result;
}

void print_grid(vector<vector<bool>> &grid) {
    for(auto row: grid) {
        for(auto col: row) {
            cout << (col ? 'X' : '.');
        }
        cout << endl;
    }
}

int visit(vector<vector<bool>> &grid, int row, int col) {
    // Validate that the indexes are in range.
    if(row < 0) return 0;
    if(col < 0) return 0;
    if(col >= (int)grid[0].size()) return 0;
    if(row >= (int)grid.size()) return 0;
    
    int count = 0;
    
    if(grid[row][col]) return 0;

    // Visit this node
    count++;
    grid[row][col] = true;

    // Visit Adjacent nodes
    count += visit(grid, row + 1, col);
    count += visit(grid, row - 1, col);
    count += visit(grid, row, col + 1);
    count += visit(grid, row, col - 1);

    return count;
}

int problem2(vector<vector<int>> &lines) {
    vector<vector<bool>> visited;
    for(auto row : lines) {
        vector<bool> this_row;
        for(auto col : row) {
            this_row.push_back(col >= 9);
        }
        visited.push_back(this_row);
    }

    vector<int> results;
    for(size_t row = 1; row < visited.size() - 1; row++) {
        for(size_t col = 1; col < visited[row].size() - 1; col++) {
            int temp = visit(visited, row, col);
            if(temp > 0)  {
                results.push_back(temp);
            }
        }
    }
    sort(results.begin(), results.end());
    return results[results.size() - 1] * results[results.size() - 2] * results[results.size() - 3];
}

int main() {
    vector<vector<int>> data;
    string filename = "puzzle_input.txt";
    ifstream input_file(filename);
    string line;

    if(!input_file.is_open()) {
        cerr << "Could not open " << filename << endl;
        return EXIT_FAILURE;
    }

    int line_length = 0;
    while(getline(input_file, line)) {
        line_length = line.size();
        vector<int> this_data;
        this_data.push_back(10);
        for(auto c : line) {
            this_data.push_back((int)c - int('0'));
        }
        this_data.push_back(10);
        data.push_back(this_data);
    }

    vector<int> boarder(line_length + 2, 10);
    data.push_back(boarder);
    data.insert(data.begin(), boarder);

    cout << "Running Problem 1" << endl;
    int result = problem1(data);
    cout << "Problem 1 Result: " << result << endl;

    cout << "Running Problem 2" << endl;
    result = problem2(data);
    cout << "Problem 2 Result: " << result << endl;

    return 0;
}