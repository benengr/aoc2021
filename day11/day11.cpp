#include <fstream>
#include <string>
#include <vector>
#include <iostream>

using namespace std;

typedef vector<vector<int>> problem_input;

void increment(problem_input &data) {
    for(size_t row = 0; row < data.size(); row++) {
        for(size_t col = 0; col < data[row].size(); col++) {
            data[row][col]++;
        }
    }
}

int check_for_flash(problem_input &data) {
    int count = 0;
    for(size_t row = 0; row < data.size(); row++) {
        for(size_t col = 0; col < data[row].size(); col++) {
            if(data[row][col] >= 10) {
                data[row][col] = 0;
                count++;

                // Row Above
                for(int nrow = (int)row - 1; nrow <= (int)row + 1; nrow++) {
                    if((nrow >= 0) && (nrow < (int)data.size())) {
                        for(int ncol = (int)col - 1; ncol <= (int)col + 1; ncol++) {
                            if((ncol == (int)col) && (nrow == (int)row)) continue;
                            if((ncol >= 0) && (ncol < (int)data[row].size())) {
                                if(data[nrow][ncol] > 0) data[nrow][ncol]++;
                            }
                        }    
                    }
                }
            }
        }
    }

    return count;
}

void print_grid(problem_input &lines) {
    for(auto row: lines) {
        for(auto col: row) {
            cout << col << " ";
        }
        cout << endl;
    }
    cout << endl;
}

int problem1(problem_input &lines) {
    const int STEPS = 100;

    int count = 0;
    for(int step = 0; step < STEPS; step++) {
        // First Increment everything
        if(step <= 10) {
            cout << "Step " << step << endl;
            print_grid(lines);
        }
        increment(lines);
        int this_step = 0;
        int this_count = check_for_flash(lines);
        while(this_count > 0) {
            this_step += this_count;
            this_count = check_for_flash(lines);
        }
        if(this_step == 100) {
            cout << "Part 2 Answer " << this_step + 1 << endl;
        }
        count += this_step;
        cout << "Step " << step + 1 << "\t" << this_step << "\t" << count << endl;
        
    }

    bool done = false;
    int step = 100;
    while(!done) {
        step += 1;
        int this_step = 0;
        increment(lines);
        int this_count = check_for_flash(lines);
        while(this_count > 0) {
            this_step += this_count;
            this_count = check_for_flash(lines);
        }
        cout << "Step " << step << "\t" << this_step << endl;
        if(this_step == 100) {
            cout << "problem 2: " << step << endl;
            done = true;
        }
    }



    return count;
}

int problem2(problem_input &lines) {
    return 0;
}

problem_input read_file(bool example) {
    problem_input lines;
    string filename = example ? "example_input.txt" : "puzzle_input.txt";
    ifstream input_file(filename);
    string line;

    if(!input_file.is_open()) {
        cerr << "Could not open " << filename << endl;
        exit(1);
    }

    while(getline(input_file, line)) {
        vector<int> row;
        for(auto c : line) {
            row.push_back((int)c - (int)'0');
        }
        lines.push_back(row);
    }
    return lines;
}

int main() {
    auto lines = read_file(false);

    cout << "Running Problem 1" << endl;
    int result = problem1(lines);
    cout << "Problem 1 Result: " << result << endl;

    return 0;
}