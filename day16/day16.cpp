#include <fstream>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

typedef vector<bool> puzzle_input;
using ull_int = unsigned long long int;

enum DECODE_STATE {
  VERSION,
  TYPE,
  LITERAL,
  OPERATOR,
  OPERAND_LENGTH_BITS,
  OPERAND_LENGTH_PACKETS,
};

struct Packet {
  uint8_t version;
  uint8_t type;
  ull_int literal;
  vector<Packet> sub_packet;
};

int bits_to_int(vector<bool> &data, int count, bool erase=true) {
  int result = 0;
  for(int i = 0; i < count; i++) {
    result = result * 2;
    if(data[i]) result++;
  }
  if(erase) data.erase(data.begin(), data.begin() + count);
  return result;
}

Packet bits_to_packet(vector<bool> &data) {
  Packet p;
  p.version = bits_to_int(data, 3);
  p.type = bits_to_int(data, 3);
  p.literal = 0;

  if(p.type == 4) {
    bool done = false;
    ull_int value = 0;
    while(!done) {
      done = !data.front();
      data.erase(data.begin());
      int temp = bits_to_int(data, 4);
      value = (value << 4) + temp;
    }
    p.literal = value;
    return p;
  } else {
    bool length_type = data.front();
    data.erase(data.begin());
    if(length_type) {
      int packet_count = bits_to_int(data, 11);
      for(int i = 0; i < packet_count; i++) {
        p.sub_packet.push_back(bits_to_packet(data));
      }
    } else {
      int bit_count = bits_to_int(data, 15);
      size_t curr_size = data.size();
      while(data.size() > curr_size - bit_count) {
        p.sub_packet.push_back(bits_to_packet(data));
      }
    }
  }
  return p;
}

Packet bits_to_packet_2(const vector<bool> &data, size_t &index) {
  DECODE_STATE state = VERSION;

  Packet result;
  int version = 0;
  int type = 0;
  int literal = 0;
  int temp_lit = 0;
  bool lit_done = false;
  result.literal = -1;
  size_t length = 0;
  size_t end_index = 0;
  while (index < data.size()) {
    switch (state) {
    case VERSION:
      version = 0;
      if (data[index])
        version += 4;
      if (data[index + 1])
        version += 2;
      if (data[index + 2])
        version += 1;
      index += 3;
      result.version = version;
      state = TYPE;
      break;
    case TYPE:
      if (data[index])
        type += 4;
      if (data[index + 1])
        type += 2;
      if (data[index + 2])
        type += 1;
      index += 3;
      result.type = type;
      if (type == 0x4)
        state = LITERAL;
      else
        state = OPERATOR;
      break;
    case LITERAL:
      if (data[index + 1])
        temp_lit += 0x8;
      if (data[index + 2])
        temp_lit += 0x4;
      if (data[index + 3])
        temp_lit += 0x2;
      if (data[index + 4])
        temp_lit += 0x1;
      lit_done = !data[index];
      index += 5;
      literal = (literal << 4) + temp_lit;
      if (lit_done) {
        result.literal = literal;
        return result;
      }
      break;
    case OPERATOR:
      state = data[index] ? OPERAND_LENGTH_PACKETS : OPERAND_LENGTH_BITS;
      index++;
      break;
    case OPERAND_LENGTH_BITS:
      length = 0;
      for (int i = 0; i < 15; i++) {
        length = length * 2;
        if (data[index])
          length++;
        index++;
      }
      end_index = index + length;
      while (index < end_index) {
        if(end_index - index < 6) {
          index = end_index;
        } else {
          result.sub_packet.push_back(bits_to_packet_2(data, index));
        }
      }
      return result;
    case OPERAND_LENGTH_PACKETS:
      length = 0;
      for (int i = 0; i < 11; i++) {
        length = length * 2;
        if (data[index])
          length++;
        index++;
      }
      while(result.sub_packet.size() < length) {
        result.sub_packet.push_back(bits_to_packet_2(data, index));
      }
      return result;
    }
  }
  return result;
}

ull_int count_packets(const Packet &p) {
  int count = p.version;
  for(auto c: p.sub_packet) {
    count += count_packets(c);
  }
  return count;
}

ull_int count_operands(const Packet &p, int level) {
  if(p.type == 0x4) return p.literal;

int i = 0;
  ull_int result;
  switch(p.type) {
  case 0:
    result = 0;
    
    for(auto c : p.sub_packet) {
      ull_int temp = count_operands(c, level+1);
      if(level == 0) {
        cout << temp << endl;
      }
      i++;
      result += temp;
      
    }
    break;
  case 1:
    result = 1;
    for (auto c: p.sub_packet) {
      result *= count_operands(c, level+1);
    }
    break;
  case 2:
    result = count_operands(p.sub_packet[0], level+1);
    for(auto c: p.sub_packet) {
      ull_int temp = count_operands(c, level+1);
      if (temp < result) result = temp;
    }
    break;
  case 3:
    result = count_operands(p.sub_packet[0], level+1);
    for(auto c: p.sub_packet) {
      ull_int temp = count_operands(c, level+1);
      if(temp > result) result = temp;
    }
    break;
  case 5:
    if (count_operands(p.sub_packet[0], level+1) > count_operands(p.sub_packet[1], level+1)) {
      result = 1;
    } else {
      result = 0;
    }
    break;
  case 6:
    if(count_operands(p.sub_packet[0], level+1) < count_operands(p.sub_packet[1], level+1)) {
      result = 1;
    } else {
      result = 0;
    }
    break;
  case 7:
    if(count_operands(p.sub_packet[0], level+1) == count_operands(p.sub_packet[1], level+1)) {
      result = 1;
    } else {
      result = 0;
    }
    break;
  }
  return result;
}

ull_int problem1(puzzle_input &data) { 
  Packet p = bits_to_packet(data);
  ull_int result = count_operands(p, 0);

  cout << "Problem 2: " << result << endl;
  return count_packets(p);
}


puzzle_input read_file(bool example) {
  vector<bool> data;
  string filename = example ? "example_input.txt" : "puzzle_input.txt";
  ifstream input_file(filename);
  string line;

  if (!input_file.is_open()) {
    cerr << "Could not open " << filename << endl;
    exit(1);
  }

  while (getline(input_file, line)) {
    for (auto c : line) {
      int temp = 0;
      if (c <= '9')
        temp = (int)c - (int)'0';
      else
        temp = (int)c - (int)'A' + 10;

      data.push_back(temp & 0x8);
      data.push_back(temp & 0x4);
      data.push_back(temp & 0x2);
      data.push_back(temp & 0x1);
    }
  }
  return data;
}

int main() {
  auto lines = read_file(false);

  cout << "Running Problem 1" << endl;
  ull_int result = problem1(lines);
  cout << "Problem 1 Result: " << result << endl;

  return 0;
}