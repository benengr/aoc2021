#include <fstream>
#include <string>
#include <vector>
#include <iostream>

using namespace std;
using ull_int = unsigned long long int;

typedef vector<string> puzzle_input;

ull_int problem1(puzzle_input &data) {
    return 0;
}

ull_int problem2(puzzle_input &data) {
    return 0;
}

puzzle_input read_file(bool example) {
    vector<string> lines;
    string filename = example ? "example_input.txt" : "puzzle_input.txt";
    ifstream input_file(filename);
    string line;

    if(!input_file.is_open()) {
        cerr << "Could not open " << filename << endl;
        exit(1);
    }

    while(getline(input_file, line)) {
        lines.push_back(line);
    }
}

int main() {
    auto lines = read_file(true);

    cout << "Running Problem 1" << endl;
    ull_int result = problem1(lines);
    cout << "Problem 1 Result: " << result << endl;

    cout << "Running Problem 2" << endl;
    result = problem2(lines);
    cout << "Problem 2 Result: " << result << endl;

    return 0;
}