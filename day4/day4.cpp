#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <deque>

using namespace std;

class BingoBoard {
public:
    BingoBoard(vector<vector<int>> board) {
        for(int row = 0; row < 5; row++) {            
            for(int col = 0; col < 5; col++) {
                spaces[row][col] = board[row][col];
                checked[row][col] = false;
            }
        }
    }

    bool CallNumber(int n) {
        int row = 0; 
        int col = 0;
        last = n;
        for(row = 0; row < 5; row++) {
            for(col = 0; col < 5; col++) {
                if(spaces[row][col] == n) {
                    checked[row][col] = true;
                    // Now Check to see if it's a bingo
                    bool foundRow  = true;
                    for(int i = 0; i < 5; i++) {
                        if(!checked[row][i]) {
                            foundRow = false;
                            break;
                        }
                    }
                    bool foundCol = true;
                    for(int i =0; i < 5; i++) {
                        if(!checked[i][col]) {
                            foundCol = false;
                            break;
                        };
                    }
                    return foundRow || foundCol;
                }
            }
        }
        
        return false;
    }

    int GetScore() {
        int result;
        for(int row = 0; row < 5; row++) {
            for(int col = 0; col < 5; col++) {
                if(!checked[row][col]) {
                    result += spaces[row][col];
                }
            }
        }
        return result * last;
    }


private:
    int spaces[5][5];
    bool checked[5][5];
    int last = -1;
};

vector<int> string_to_vector(string &input) {
    vector<int> result;
    int digit = 0;
    bool digitFound = false;
    for(size_t i = 0; i < input.size(); i++) {
        if((input[i] == ',') || (input[i] == ' ')) {
            if(digitFound) {
                result.push_back(digit);
                digit = 0;
                digitFound = false;
            }
        } else if((input[i] >= '0') && (input[i] <= '9')) {
            digit = digit * 10;
            digit += (int)input[i] - (int)'0';
            digitFound = true;
        }        
    }
    if(digitFound) result.push_back(digit);
    return result;
}

int problem1(vector<string> lines) {
    vector<int> called = string_to_vector(lines[0]);
    vector<BingoBoard> boards;
    for(size_t i = 1; i < lines.size() - 5; i += 6) {
        vector<vector<int>> board;
        for(int j = 1; j <= 5; j++) {
            vector<int> row = string_to_vector(lines[i + j]);
            board.push_back(row);
        }
        boards.push_back(BingoBoard(board));
        board.clear();
    }

    vector<BingoBoard> winners;
    int call_index = 0;
    while(winners.size() == 0) {
        int n = called[call_index];
        call_index++;
        for(size_t b = 0; b < boards.size(); b++) {
            if(boards[b].CallNumber(n)) {
                winners.push_back(boards[b]);
            }
        }
    }

    cout << "There was " << winners.size() << " winning boards" << endl;

    int result = 0;
    for(size_t i = 0; i < winners.size(); i++) {
        int current_result = winners[i].GetScore();
        if(current_result > result) result = current_result;
    }
    return result;
}

int problem2(vector<string> lines) {
    vector<int> called = string_to_vector(lines[0]);
    vector<BingoBoard> boards;
    for(size_t i = 1; i < lines.size() - 5; i += 6) {
        vector<vector<int>> board;
        for(int j = 1; j <= 5; j++) {
            vector<int> row = string_to_vector(lines[i + j]);
            board.push_back(row);
        }
        boards.push_back(BingoBoard(board));
        board.clear();
    }

    int call_index = 0;
    while(boards.size() > 1) {
        int n = called[call_index];
        call_index++;
        deque<size_t> erase_list;

        for(size_t b = 0; b < boards.size(); b++) {
            if(boards[b].CallNumber(n)) {
                erase_list.push_front(b);
            }
        }
        for(auto i : erase_list) {
            boards.erase(boards.begin() + i);
        }
    }

    cout << "There was " << boards.size() << " losing boards" << endl;

    // Now call Until it wins
    while(call_index < (int)called.size()) {
        int n = called[call_index];
        if(boards[0].CallNumber(n)) {
            return boards[0].GetScore();
        }
        call_index++;
    }
    
    return 0;
}

int main() {
    string filename("puzzle_input.txt");
    vector<string> lines;
    string line;

    ifstream input_file(filename);

    if(!input_file.is_open()) {
        cerr << "Could not open " << filename << endl;
        return EXIT_FAILURE;
    }

    while(getline(input_file, line)) {
        lines.push_back(line);
    }

    cout << "running problem 1" << endl;
    int result = problem1(lines);
    cout << "problem 1 result: " << result << endl;

    cout << "running problem 2" << endl;
    result = problem2(lines);
    cout << "problem 2 result " << result << endl;
    return 0;
}
