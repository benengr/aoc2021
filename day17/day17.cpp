#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <math.h>

using namespace std;

struct puzzle_input {
    int min_x;
    int max_x;
    int min_y;
    int max_y;
};

double solve_quadratic(double a, double b, double c) {
    double sq = sqrt(b * b - 4 * a * c);
    if(sq > 0) {
        return (-b + sq) / (2 * a);
    } else {
        return (-b - sq) / (2 * a);
    }
}

long long problem1(puzzle_input &data) {
    /*************************************************
     * If y0 is > 0, it will reach 0 at step y0 * 2 + 1 and the next step will have a velocity of -y0.
     * 
     * That means that the maximum possible y0 value is -max_y;
     */

    long long y0 = -data.min_y;
    while(y0 > 0) {
        int steps = y0 * 2 + 1; // The total number of steps to get to 0;
        int y = 0;
        int y_vel = -y0;
        while(y > data.max_y) {
            steps++;
            y_vel--;
            y = y + y_vel;
        }

        // After the first part we are now greater than the min_y, we can 
        // continue to find a solution for x while we are <= max_y (still in zone)
        while(y >= data.min_y) {
            // If x < steps then the total distance traveled is (x + 1) * (x) / 2.
            // Can we find a value that will fall between min/max x?
            double smallest = solve_quadratic(1, 1, -2 * data.min_x);

            int x = ceil(smallest);
            if(x < steps) {
                return (long long)((double)y0 + 1) * ((double)y0) / 2;
            } else {
                cout << "boo";
            }
        }
        y0--;
    }

    return -1;
}

int can_hit_x(int minx, int maxx, int steps) {
    // The smallest x we need to check is smallest value that can
    // get us to minx
    double smallest = solve_quadratic(1, 1, -2 * minx);
    int biggest = maxx;

    int count = 0;
    int x = ceil(smallest);

    while(x <= biggest) {
        int dist;
        if(x >= steps) {
            int temp = (double)steps * (double)(steps - 1) / (double)2;
            dist = steps * x - temp;
        } else {
            dist = (int)((double)(x + 1)* (double)(x) / (double(2)));
        }

        if((dist <= maxx) && (dist >= minx)) 
        {
            count++;
        }
        else if(dist > maxx) return count;
        x++;
    }
    return count;
}

long long brute(puzzle_input &data) {
    long long count = 0;
    for(int x = 0; x <= data.max_x; x++) {
        for(int y = data.min_y; y < -data.min_y; y++) {
            int step = 0;
            int dx = 0;
            int dy = 0;
            int vx = x;
            int vy = y;

            do {
                step++;
                dx += vx;
                dy += vy;
                if(vx > 0) vx--;
                vy--;

                if((dx >= data.min_x) && (dx <= data.max_x)) {
                    if((dy >= data.min_y) && (dy <= data.max_y)) {
                        count++;
                        break;
                    }
                }
            } while((dx <= data.max_x) && (dy >= data.min_y));
        }
    }
    return count;
}

long long problem2(puzzle_input &data) {
    int count = 0;
    int y = data.min_y;

    while(y <= -data.max_y) {
        int step = 0;
        int current_y = 0;
        int y_vel = y;
        if(y > 0) {
            step = y * 2 + 1;
            y_vel = -y;
        }

        while(current_y >= data.min_y) {
            if(current_y <= data.max_y) {
                count += can_hit_x(data.min_x, data.max_x, step);
            }

            // Do the next step
            step++;
            current_y += y_vel;
            y_vel--;
        }

        y++;
    }

    return count;
}

puzzle_input read_file(bool example) {
    if(example) {
        return {
            20, 30, -10, -5
        };
    } else {
        return {
            217, 240, -126, -69
        };
    }
}

int main() {
    auto lines = read_file(false);

    cout << "Running Problem 1" << endl;
    long long result = problem2(lines);
    cout << "Problem 1 Result: " << result << endl;

    cout << "Running Problem 2" << endl;
    result = brute(lines);
    cout << "Problem 2 Result: " << result << endl;

    return 0;
}