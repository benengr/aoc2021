#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <map>
#include <algorithm>
#include <set>

using namespace std;

struct Node {
    char val;
    Node *next;
    Node *prev;
};

struct Rule {
    char first;
    char second; 
    char insert;
};

struct RuleNodeId {
    char id1;
    char id2;

    bool operator==(const RuleNodeId &rhs) const {
        return ((id1 == rhs.id1) && (id2 == rhs.id2));
    }

    bool operator<(const RuleNodeId &rhs) const {
        if(id1 == rhs.id1) return id2 < rhs.id2;
        return id1 < rhs.id1;
    }  
};

struct RuleNode {
    char insert;
    RuleNode *lhs;
    RuleNode *rhs;
};

struct puzzle_input {
    Node* root;
    vector<Rule> rules;
    map<char, long long> counts;
};

struct puzzle_input_2 {
    string start;
    map<char, long long> counts;
    map<RuleNodeId, RuleNode*> rules;
};



void print_chain(Node *root) {
    for(Node *current = root; current != NULL; current = current->next) {
        cout << current->val;
    }
    cout << endl;
}

void iterate(char a, char b, int count, vector<Rule> &rules, map<char, long long> &counts) {
    if(count == 0) return;

    for(auto r : rules) {
        if((a == r.first) && (b == r.second)) {
            counts[r.insert]++;
            iterate(a, r.insert, count - 1, rules, counts);
            iterate(r.insert, b, count - 1, rules, counts);
            return;
        }
    }
}

void iterate(Node *root, vector<Rule> &rules, map<char, long long> &counts) {
    for(Node *current = root->next; current != NULL; current = current->next) {
        for(auto rule : rules) {
            if((current->prev->val == rule.first) && (current->val == rule.second)) {
                Node *n = new Node;
                n->val = rule.insert;
                n->prev = current->prev;
                n->next = current;

                current->prev->next = n;
                current->prev = n;

                counts[rule.insert]++;
                break;
            }
        }
    }
}

bool sort_node(const pair<char, long long> &lhs, const pair<char, long long> &rhs) {
    return lhs.second < rhs.second;
}

int problem1(puzzle_input &data) {
    for(int iteration = 0; iteration < 10; iteration++) {
        if(iteration < 5) {
            cout << "Before Iteration " << iteration+1 << ": ";
            print_chain(data.root);
        } else {
            cout << "Running iteration " << iteration+1 << endl;
        }
        
        iterate(data.root, data.rules, data.counts);
    }

    vector<pair<char, long long>> A;
    for(auto &it : data.counts) {
        A.push_back(it);
    }
    sort(A.begin(), A.end(), sort_node);
    auto min = A.begin();
    auto max = A.end() - 1;
    cout << "Min Count: " << (*min).first << "/" << (*min).second << endl;
    cout << "Max Count: " << (*max).first << "/" << (*max).second << endl;
    return (*max).second - (*min).second;
    
}

/****************************************************************************************
 * Run the process 40 times
 * 
 * To do this we will keep track of how many times each rule is applied.  Then the counts
 * of each letter are the inital counts from the start, plus the number of times each
 * letter gets inserted
 * Count[n] = inital + Sum of number of times a rule that inserted that character was used.
 * 
 * For each iteration, we can go through each rule that will be used in that iteration, the number of tmies that rule
 * will be used and figure out which rules will be called the next time.
 * 
 * NNCB
 * The first iteration will use the following rules:
 *  NN NC CB
 * and each of those rules will be used one time.
 * After each iteration, the number of times a rule is used is:
 * 1. The Number of times it was used before this iteration
 * 2. Plus the number of times it is used IN this iteration
 * 
 * To do this we will keep track of the following:
 *  1. The cumulative number of times a rule has been visited
 *  2. The rules that are going to be visited next iteration
 *  3. The rules to visit this iteration.
 */
long long problem2(puzzle_input_2 &data) {
    // We will keep a count of how many times we are using each rule on each iteration;
    map<RuleNodeId, long long> ruleNodeCounts;
    map<RuleNodeId, long long> currentNodeCounts;

    // Setup the inital counts
    data.counts[data.start[0]] = 1;
    // From the string, get the inital counts and the rules that will be applied next iteration
    for(size_t i = 1; i < data.start.size(); i++) {
        RuleNodeId id = {data.start[i - 1], data.start[i]};
        currentNodeCounts[id]++;
        data.counts[data.start[i]] += 1;
    }

    for(int it = 1; it <= 40; it++) {
        // Keep track of how many times we will visit each rule next iteration
        map<RuleNodeId, long long> nextNodeCounts;

        for(auto r : currentNodeCounts) {
            ruleNodeCounts[r.first] += r.second; // Keep the tally of the cumulative number of times this rule has been used.

            auto a = data.rules[r.first];

            // Determine the rules that will be visited because of this rule
            RuleNodeId left = {r.first.id1, a->insert};
            RuleNodeId right = {a->insert, r.first.id2};

            // Incrment the number of times this rule will be visited by how many times the current rule
            // is visited.
            nextNodeCounts[left] += r.second;
            nextNodeCounts[right] += r.second;
        }
        currentNodeCounts.swap(nextNodeCounts);
    }

    for(auto r : ruleNodeCounts) {
        // Now we have the cumultive number of times each rule has been visited.  Get the rule from storage.
        auto l = data.rules[r.first];
        
        // Now increment the inserted character by the # of times the rule was applied
        data.counts[l->insert] += r.second;
    }

    vector<pair<char, long long>> A;
    for(auto &it : data.counts) {
        A.push_back(it);
    }
    sort(A.begin(), A.end(), sort_node);
    auto min = A.begin();
    auto max = A.end() - 1;
    cout << "Min Count: " << (*min).first << "/" << (*min).second << endl;
    cout << "Max Count: " << (*max).first << "/" << (*max).second << endl;
    return (*max).second - (*min).second;
}

puzzle_input read_file(bool example) {
    puzzle_input result;
    string filename = example ? "example_input.txt" : "puzzle_input.txt";
    ifstream input_file(filename);
    string line;

    if(!input_file.is_open()) {
        cerr << "Could not open " << filename << endl;
        exit(1);
    }

    // Get the nodes;
    getline(input_file, line);
    Node* prev;
    for(auto c : line) {
        Node* current = new Node;
        current->prev = prev;
        current->next = NULL;
        current->val = c;
        if(prev == NULL) {
            result.root = current;
            prev = result.root;
        } else {
            prev->next = current;
        }
        prev = current;
        result.counts[c]++;
    }
    getline(input_file, line); // The blank line

    while(getline(input_file, line)) {
        Rule r;
        r.first = line[0];
        r.second = line[1];
        r.insert = line[6];
        result.rules.push_back(r);
    }
    return result;
}


puzzle_input_2 read_file_2(bool example) {
    puzzle_input_2 result;
    string filename = example ? "example_input.txt" : "puzzle_input.txt";
    ifstream input_file(filename);
    string line;

    if(!input_file.is_open()) {
        cerr << "Could not open " << filename << endl;
        exit(1);
    }

    // Get the nodes;
    getline(input_file, line);
    result.start = line;

    getline(input_file, line); // The blank line

    while(getline(input_file, line)) {
        RuleNode* r = new RuleNode;
        RuleNodeId id;
        id.id1 = line[0];
        id.id2 = line[1];
        r->insert = line[6];

        RuleNodeId left;
        RuleNodeId right;

        left.id1 = id.id1;
        left.id2 = r->insert;
        right.id1 = r->insert;
        right.id2 = id.id2;

        result.rules[id] = r;
        auto ptr = result.rules.find(left);
        if(ptr != result.rules.end()) {
            r->lhs = (*ptr).second;
        }
        ptr = result.rules.find(right);
        if(ptr != result.rules.end()) {
            r->rhs = (*ptr).second;
        }

        // Now we have to go through all current rules and point to this one if the should;
        for(auto it: result.rules) {
            RuleNodeId left;
            left.id1 = it.first.id1;
            left.id2 = it.second->insert;

            if(left == id) {
                it.second->lhs = r;
            }

            right.id1 = it.second->insert;
            right.id2 = it.first.id2;
            if(right == id) {
                it.second->rhs = r;
            }
        }
    }
    return result;
}

int main() {
    auto lines = read_file(false);

    cout << "Running Problem 1" << endl;
    long long result = problem1(lines);
    cout << "Problem 1 Result: " << result << endl;

    auto lines2 = read_file_2(false);
    cout << "Running Problem 2" << endl;
    result = problem2(lines2);
    cout << "Problem 2 Result: " << result << endl;

    return 0;
}