#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

int bin_to_int(string s) {
    int result = 0;
    for(size_t i = 0; i < s.size(); i++) {
        result *= 2;
        if(s[i] == '1') {
            result += 1;
        }
    }
    return result;
}
int part_a(vector<string> &data) {
    vector<int> counts(data[0].size(), 0);
    vector<string> oxy;
    vector<string> co2;

    for(auto s: data) {
        oxy.push_back(s);
        co2.push_back(s);
        // Index 0 is the Most Significant Bit
        for(int i = 0; i < (int)s.size(); i++) {
            if(s[i] == '1') {
                counts[i]++;
            }
        }
    }

    int ep = 0;
    int ga = 0;
    for(int i = 0; i < (int)counts.size(); i++) {
        ep *= 2;
        ga *= 2;
        if(counts[i] > (int)data.size() / 2) {
            // There were more 1s than 0s
            ep += 1;
        } else {
            ga += 1;
        }
    }


    for(size_t i = 0; i < counts.size(); i++) {
        char filt = counts[i] >= (int)data.size() / 2 ? '1' : '0';
        size_t j = 0;
        while((j < oxy.size()) && (oxy.size() > 1)) {
            if(oxy[j][i] != filt) {
                oxy.erase(oxy.begin() + j);
            } else {
                j++;
            }
        } 
        j = 0;
        while ((co2.size() > 1) && (j < co2.size())) {
            if(co2[j][i] == filt) {
                co2.erase(co2.begin() + j);
            } else {
                j++;
            }
        }
    }
    cout << "  ep: " << ep << endl;
    cout << "  ga: " << ga << endl;

    int o1 = bin_to_int(oxy[0]);
    int c1 = bin_to_int(co2[0]);
    if(oxy.size() > 1) {
        cout << "too many oxy" << endl;
    } else {
        cout << "oxy " << oxy[0] << " " << o1 << endl;
    }

    if(co2.size() > 1) {
        cout << "too many co2" << endl;    
    } else {
        cout << "co2 " << co2[0] << " " << c1 << endl;
    }

    cout << "part b result: " << c1 * o1 << endl;


    return ep * ga;
}

bool sort_function(string &lhs, string &rhs) {
    return lhs[0] < rhs[0];
}

vector<string>::iterator find_transition(vector<string>::iterator start, vector<string>::iterator end) {
    // Binary search for the transition
    vector<string>::iterator mid;
    while(start <= end) {
        mid = start + (distance(start, end) / 2);

        // Is this the transition
        if(mid == start) {
            if((*start)[0] == '1') return start;
            start = mid + 1;
        } else {
            // Are we low
            char c = (*mid)[0];
            char prev = (*(mid - 1))[0];
            if(c == '0') {
                start = mid + 1;
            } else if(prev == '1') {
                end = mid - 1;
            } else {
                return mid;
            }
        } 
    }    
    return mid;
}

int recurse(vector<string>::iterator start, vector<string>::iterator end, bool most) {

    sort(start, end);
    int total_dist = distance(start, end);
    auto index = find_transition(start, end);
    if(index == start) {
        return bin_to_int(*start);
    } else if(index == end - 1) {
        return bin_to_int(*(end - 1));
    } else {
        int dist = distance(start, index);
        // Change the vector and then find the next;
        if(most) {
            if(dist > total_dist / 2) {
                return recurse(start, index, most);
            } else {
                return recurse(index, end, most);
            }
        } else {
            if(dist > total_dist / 2) {
                return recurse(index, end, most);
            } else {
                return recurse(start, index, most);
            }
        }
    }

}

char count_index(vector<string> &data, int index, bool most) {
    size_t count = 0;
    for(auto s : data) {
        if (s[index] == '1') count++;
    }
    float target = (float)data.size() / 2;
    if(count < target) {
        // There are fewer 1's here
        return most ? '0' : '1';
    } else if (count > target) {
        // There are more 0's here
        return most ? '1' : '0';
    } else {
        return most ? '1' : '0';
    }
}

int part_b(vector<string> &data) {
    vector<string> uut(data.size(), "");
    copy(data.begin(), data.end(), uut.begin());

    int index = 0;
    while(uut.size() > 1) {
        char filt = count_index(uut, index, true);
        size_t i = 0;
        while(i < uut.size()) {
            if(uut[i][index] != filt) {
                uut.erase(uut.begin() + i);
            } else  {
                i++;
            }
        }
        index++;
    }
    int oxy = bin_to_int(uut[0]);
    cout << "oxy " << oxy << endl;
    vector<string> uut2(data.size(), "");
    copy(data.begin(), data.end(), uut2.begin());
    index = 0;
    while(uut2.size() > 1) {
        char filt = count_index(uut2, index, false);
        size_t i = 0;
        while(i < uut2.size()) {
            if(uut2[i][index] != filt) {
                uut2.erase(uut2.begin() + i);
            } else  {
                i++;
            }
        }
        index++;
    }
    int co2 = bin_to_int(uut2[0]);
    cout << "co2 " << co2 << endl;
    return oxy * co2;

}

int main() {
    ifstream data_file("problem_input.txt");
    vector<string> data;
    string temp;

    while(getline(data_file, temp)) {
        data.push_back(temp);
    }

    cout << "Running part a" << endl;
    int result = part_a(data);
    cout << "Part 1: " << result << endl;
    result = part_b(data);
    cout << "Part 2: " << result << endl;
    return 0;
}