| Number | Control Signals | 
|---|---|
| 0 | 6 |
| 1 | 2 |
| 2 | 5 | 
| 3 | 5 |
| 4 | 4 | 
| 5 | 5 | 
| 6 | 6 |
| 7 | 3 | 
| 8 | 7 |
| 9 | 6 |

The following digits use a unique number of control signals:
1, 7, 4, 8

* If we have a 1 and a 7, we can tell which signal controls the top of the display

* Based on the size of the string:

if (s == 2) {
    return 1;
} else if (s == 3) {
    return 7;
} else if (s == 4) {
    return 4;
} else if (s == 5) {
    if the value contains both the values from s == 2, return 3;
    if the value contains the two values from s == 4 that is not in s == 2, return 5;
    else return 2;
} else if (s == 6) { // 0, 6, or 9
    if the value does not contain both the values of s == 4 not in s == 2, return 0;
    if the value does not contain everything from s == 2, return 6;
    else return 9;
} else {
    return 8; // 
}

* Things to keep track of:
1. When s == 4, which two values are NOT in s == 2;