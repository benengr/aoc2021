#include <fstream>
#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

bool sort_func(string &rhs, string &lhs) {
    return rhs.size() < lhs.size();
}

bool both_from_2(string &current, string &size_2) {
    int found_count = 0;
    for(auto c : current) {
        for(size_t i = 0; i < 2; i++) {
            if(c == size_2[i]) {
                found_count++;
                if(found_count == 2) return true;
                break;
            }
        }
    }
    return false;
}



int get_from_digits(string &current, string &not_in_2, string &size_2) {
    if(current.size() == 2) return 1;
    else if(current.size() == 3) return 7;
    else if(current.size() == 4) return 4;
    else if(current.size() == 5) {
        if(both_from_2(current, size_2)) return 3;
        else if(both_from_2(current, not_in_2)) return 5;
        return 2;
    }
    else if(current.size() == 6) {
        if(!both_from_2(current, not_in_2)) return 0;
        else if(!both_from_2(current, size_2)) return 6;
        return 9;
    } else {
        return 8;
    }
}

int main() {
    ifstream data_file("puzzle_input.txt");
    string temp;

    int count = 0;
    int p2Sum = 0;
    while(getline(data_file, temp)) {
        // find the pipe
        size_t i = 0;
        string s;
        vector<string> digits;
        while(temp[i] != '|') {
            if(temp[i] != ' ') {
              s += temp[i];
            } else {
                digits.push_back(s);
                s.clear();
            }
            i++;
        }
        sort(digits.begin(), digits.end(), sort_func);
        string not_in_2;
        for(size_t i = 0; i < digits[2].size(); i++) {
            char c = digits[2][i];
            bool found = false;
            for(size_t j = 0; j < digits[0].size(); j++) {
                if(c == digits[0][j]) {
                    found = true;
                    break;
                }
            }
            if(!found) {
                not_in_2 += c;
            }
        }
        i++; // Space after the pipe;
        i++;
        int letterCount = 0;
        int digit = 0;
        while(i < temp.size()) {
            if(temp[i] != ' ') {
                s += temp[i];
                letterCount++;  
            } else {
                if((letterCount == 2) || (letterCount == 3) || (letterCount == 4) || (letterCount == 7)) {
                    count++;
                }
                int current = get_from_digits(s, not_in_2, digits[0]);
                digit = (digit * 10) + current;
                s.clear();
                letterCount = 0;
            }
            i++;
        }
        if((letterCount == 2) || (letterCount == 3) || (letterCount == 4) || (letterCount == 7)) {
            count++;
        }
        digit = digit * 10 + get_from_digits(s, not_in_2, digits[0]);
        p2Sum += digit;
    }

    cout << "Problem 1 Result: " << count << endl;
    cout << "Problem 2 result: " << p2Sum << endl;
    return 0;
}