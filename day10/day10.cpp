#include <algorithm>
#include <fstream>
#include <iostream>
#include <stack>
#include <string>
#include <vector>

using namespace std;

//const string FILENAME = "example_input.txt";
const string FILENAME = "puzzle_input.txt";

struct Result {
  int r1;
  long long r2;
};

Result problem1(vector<string> &lines) {
  int result = 0;
  vector<long long> ok;
  stack<char> s;
  for (auto line : lines) {
    while (s.size() > 0)
      s.pop();
    bool corrupt = false;

    for (auto c : line) {
      if ((c == '(') || (c == '[') || (c == '{') || (c == '<')) {
        s.push(c);
      } else {
        char expected = '(';
        int value = 3;
        switch (c) {
        case ')':
          expected = '(';
          value = 3;
          break;
        case ']':
          expected = '[';
          value = 57;
          break;
        case '}':
          expected = '{';
          value = 1197;
          break;
        case '>':
          expected = '<';
          value = 25137;
          break;
        default:
          cout << "Invalid Character " << c;
          return {-1, -1};
        }

        // We had a close character, check to see if we can match it.
        if (s.size() > 0) {
          // Is this a match?
          char open = s.top();
          if (open == expected) {
            // Yes, remove it from the stack
            s.pop();
          } else {
            // No, increment by the required amount and stop checking this
            // string
            result += value;
            corrupt = true;
            break;
          }
        } else {
          // Since there are no open characters left, this string is corrupted,
          // Increase by the close value and stop checking this string
          result += value;
          corrupt = true;
          break;
        }
      }
    }
    if (!corrupt) {
      long long comp = 0;
      while (s.size() > 0) {
        comp = comp * 5;
        auto c = s.top();
        s.pop();
        switch (c) {
        case '(':
          comp += 1;
          break;
        case '[':
          comp += 2;
          break;
        case '{':
          comp += 3;
          break;
        case '<':
          comp += 4;
          break;
        default:
          cout << "Invalid character on stack: " << c << endl;
          return {result, -1};
        }
      }
      ok.push_back(comp);
    }
  }
  sort(ok.begin(), ok.end());
  return {result, ok[ok.size() / 2]};
}

int main() {
  vector<string> lines;
  ifstream input_file(FILENAME);
  string line;

  if (!input_file.is_open()) {
    cerr << "Could not open " << FILENAME << endl;
    return EXIT_FAILURE;
  }

  while (getline(input_file, line)) {
    lines.push_back(line);
  }

  cout << "Running Problem 1" << endl;
  Result result = problem1(lines);
  cout << "Problem 1 Result: " << result.r1 << endl;
  cout << "Problem 2 Result: " << result.r2 << endl;

  return 0;
}