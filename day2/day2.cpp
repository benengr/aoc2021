#include <fstream>
#include <vector>
#include <iostream>

using namespace std;

/**************************************************************
 * Since the input is validated, we really only need to look at
 * 2 characters, the first one f, u, d and the last one 0 - 9
 * ***********************************************************/
int problem1(vector<string> &input) {
    int x = 0; // Horizontal Position
    int y = 0; // vertical position

    for(auto l : input) {
        int val = (int)l[l.size() - 1] - (int)'0';
        switch(l[0]) {
            case 'f':
                x += val;
                break;
            case 'u':
                y -= val;
                break;
            case 'd':
                y += val;
                break;
        }
    }

    cout << "  x: " << x << endl;
    cout << "  y: " << y << endl;
    return x * y;
}

int problem2(vector<string> &input) {
    int x = 0; // Horizontal Position
    int y = 0; // vertical position
    int aim = 0;

    for(auto l : input) {
        int val = (int)l[l.size() - 1] - (int)'0';
        switch(l[0]) {
            case 'f':
                x += val;
                y += aim * val;
                break;
            case 'u':
                aim -= val;
                break;
            case 'd':
                aim += val;
                break;
        }
    }

    cout << "  x: " << x << endl;
    cout << "  y: " << y << endl;
    return x * y;
}

int main() {
    string filename("puzzle_input.txt");
    vector<string> lines;
    string line;

    ifstream input_file(filename);

    if(!input_file.is_open()) {
        cerr << "Could not open " << filename << endl;
        return EXIT_FAILURE;
    }

    while(getline(input_file, line)) {
        lines.push_back(line);
    }

    cout << "Starting Problem 1" << endl;
    int result = problem1(lines);
    cout << "Problem 1 Result: " << result << endl;

    cout << "Starting Problem 2" << endl;
    result = problem2(lines);
    cout << "Problem 2 Result: " << result << endl;

    return EXIT_SUCCESS;
}