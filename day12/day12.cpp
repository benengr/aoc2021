#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <set>
#include <map>

using namespace std;

map<int, int> visit_count;

class Cave {
public:
    string name;
    int id;
    bool large;
    map<int, Cave *> connections;
    Cave(string name, int id) {
        this->name = name;
        if(name == "start") this->id = 0;
        else if((name[0] >= 'a') && (name[0] <= 'z')) this->id = id + 10000;
        else this->id = id;

        if((name[0] >= 'A') && (name[0] <= 'Z')) {
            this->large = true;
        } else {
            this->large = false;
        }
    }

    void AddConnection(Cave* connection) {
        auto current = this->connections.find(connection->id);
        if(current == this->connections.end()) {
            this->connections[connection->id] = connection;
        }
    }
};

typedef map<string, Cave*> puzzle_input;

int count_paths(Cave *root, set<int> &visited) {
    if(root->name == "end") return 1;
    if(root == NULL) return 0;

    int count = 0;
    visited.insert(root->id);
    for(auto c: root->connections) {
        if(c.second->large) {
            count += count_paths(c.second, visited);
        } else {
            if(visited.find(c.second->id) == visited.end()) {
                // We have not visited
                count += count_paths(c.second, visited);
            } 
        }
    }
    visited.erase(root->id);
    return count;
}

bool can_visit(Cave* node) {
    if (node->id < 100) return false;
    
    if(node->large) return true;

    if(visit_count[node->id] == 1) {
        map<int, int>::reverse_iterator rit;
        for(rit = visit_count.rbegin(); rit != visit_count.rend(); ++rit) {
            if((rit->first > 10000) && (rit->second >= 2)) {
                return false;
            }
            if(rit->first < 10000) return true;
        }
    }

    return visit_count[node->id] < 2;
}

int count_paths_2(Cave *root) {
    if(root->name == "end") return 1;
    if(root == NULL) return 0;

    int count = 0;

    // Insert or incrment
    visit_count[root->id]++;

    for(auto c: root->connections) {
        if(can_visit(c.second)) {
            count += count_paths_2(c.second);
        }
    }
    visit_count[root->id]--;
    return count;
}

int problem1(puzzle_input &data) {
    set<int> v;
    return count_paths(data["start"], v);
}

int problem2(puzzle_input &data) {
    return count_paths_2(data["start"]);
}

puzzle_input read_file(bool example) {
    vector<string> lines;
    string filename = example ? "example_input.txt" : "puzzle_input.txt";
    ifstream input_file(filename);
    string line;

    puzzle_input nodes;
    int id = 100;
    if(!input_file.is_open()) {
        exit(1);
    }

    while(getline(input_file, line)) {
        int index = 1;
        while(line[index] != '-') {
            index++;
        }

        string a = line.substr(0, index);
        string b = line.substr(index + 1, line.size());

        if(nodes.find(a) == nodes.end()) {
            // Insert this node;
            Cave * current = new Cave(a, id);
            nodes[a] = current;
            id++;
            visit_count[current->id] = 0;

        }

        if(nodes.find(b) == nodes.end()) {
            // Insert this node;
            Cave * current = new Cave(b, id);
            nodes[b] = current;
            id++;
            visit_count[current->id] = 0;
        }

        // Create the connections
        nodes[a]->AddConnection(nodes[b]);
        nodes[b]->AddConnection(nodes[a]);
    }
    return nodes;
}

int main() {
    auto lines = read_file(false);

    cout << "Running Problem 1" << endl;
    int result = problem1(lines);
    cout << "Problem 1 Result: " << result << endl;

    cout << "Running Problem 2" << endl;
    result = problem2(lines);
    cout << "Problem 2 Result: " << result << endl;

    return 0;
}